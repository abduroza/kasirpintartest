const express = require("express");
const app = express();

const morgan = require("morgan");
app.use(morgan("dev"));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(require("./routes/index.routes"));

app.get("/", (req, res) => {
  res.status(200).json({ message: "Hello Kasir Pintar" });
});

const port = 8000;
app.listen(port, () => {
  console.log(`Server Started at ${Date()}`);
  console.log(`Server running in http://localhost:${port}`);
});
