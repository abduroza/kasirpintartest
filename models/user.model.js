const userData = require("../data/user.json");
const helper = require("../helpers/helper");

function getUser() {
  return new Promise((resolve, reject) => {
    if (userData.length === 0) {
      reject({
        message: "no posts available",
        status: 202
      });
    }
    resolve(userData);
  });
}

function getEmail(email) {
  return new Promise((resolve, reject) => {
    helper
      .findInArray(userData, email)
      .then(user => resolve(user))
      .catch(err => reject(err));
  });
}

module.exports = { getUser, getEmail };
