const router = require("express").Router();
const User = require("../models/user.model");
const auth = require("../helpers/auth");
const bcrypt = require("bcrypt");
var jwt = require("jsonwebtoken");

router.post("/login", async (req, res) => {
  const email = req.body.email;
  await User.getEmail(email)
    .then(function(user) {
      if (!user) {
        res.status(401).json({ message: "email not match" });
      } else {
        bcrypt.compare(req.body.password, user.password, function(err, result) {
          if (result == true) {
            jwt.sign({ id: user.id, email: user.email }, "xyz", function(
              err,
              token
            ) {
              res.status(200).json({ token: token, message: "login Success" });
            });
          } else {
            res.status(400).json({ message: "Invalid Password" });
          }
        });
      }
    })
    .catch(err =>
      res.status(400).json({ error: err, message: "email not found" })
    );
});

router.get("/get", auth, async (req, res) => {
  await User.getUser()
    .then(data => res.json({ message: "kasir pintar" }))
    .catch(err => res.status(400).json({ message: err.message }));
});

module.exports = router;
