const router = require("express").Router();
const user = require("./user.routes");

router.use("/api/v1/user", user);

module.exports = router;
