var jwt = require("jsonwebtoken");

function auth(req, res, next) {
  let token = req.headers.authorization;
  if (!token) return res.status(401).json({ message: "Token Not Available" });
  try {
    jwt.verify(token, "xyz", function(err, decoded) {
      if (err)
        return res.status(401).json({ error: err, message: "Invalid Token" });
      req.user = decoded;
      next();
    });
  } catch (err) {
    res.status(401).json({ error: err, message: "Invalid Token" });
  }
}
module.exports = auth;
