const fs = require("fs");

const getNewId = array => {
  if (array.length > 0) {
    return array[array.length - 1].id + 1;
  } else {
    return 1;
  }
};

function findInArray(array, email) {
  return new Promise((resolve, reject) => {
    const row = array.find(r => r.email == email);
    if (!row) {
      reject({
        message: "Email is not found",
        status: 404
      });
    }
    resolve(row);
  });
}

function writeJSONFile(filename, content) {
  fs.writeFileSync(filename, JSON.stringify(content), "utf8", err => {
    if (err) {
      console.log(err);
    }
  });
}

module.exports = {
  getNewId,
  findInArray,
  writeJSONFile
};
