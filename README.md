### Requirements

- You need to install Node js

### How to use

1. Clone the project app from gitlab.
   `git clone https://gitlab.com/abduroza/kasirpintartest.git`

2. Go to app.
   `cd kasirpintartest`

3. Install dependency.
   `npm install`

4. Run app to start development server.
   `npm run start`

5. Open postman. Url below in postman to login app.
   `http://localhost:8000/api/v1/user/login`

6. Copy token and then paste in Headers

7. Open url below to access 2nd endpoint.
   `http://localhost:8000/api/v1/user/get`
